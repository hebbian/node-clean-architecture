import axios from 'axios'
import config from '../.env.json'

class HTTPHelper {
    async sendErrorLog(data) {
        try {
            let headers = {}
            let url = config.errorSlackWebHook
            let errorData = {
                pretext: data.pretext,
                color: '#ff0000',
                fields: data.fields
            }
            let result = await axios({
                method: 'post',
                headers: headers,
                url: url,
                data: errorData
            })
            if (result.status === 200) {
                return result
            } else {
                return null
            }
        } catch (error) {
            return null
        }
    }
}
export default new HTTPHelper()