import CartModel from '../model/Cart'
import HTTPHelper from '../../../../../helper/HTTPHelper'
import config from '../../../../../.env.json'

class CartRepository {

    async readAll(conditions = null, assoc=[], limit=10, offset=0) {
        try {
            let results = await CartModel.instance.findAll({
                where: conditions,
                order: [
                    ['createdAt', 'DESC']
                ],
                include: assoc,
                limit: limit,
                offset: offset,
                mapToModel: true,
                model: CartModel.instance
            })
            return results
        } catch(error) {
            console.log('[ERROR - READ ALL] '+error.message)
            let errorData = {
                pretext: '[ERROR - '+ config.environment.toUpperCase() +'] Read All Cart',
                color: '#ff0000',
                fields: [
                    {
                        title: 'Error GET Read All Cart',
                        value: error.message
                    }
                ]
            }
            HTTPHelper.sendErrorLog(errorData)
            return null
        }
    }

    async readOne(conditions = null, assoc=[]) {
        try {
            let results = await this.instance.find({
                where: conditions,
                include: assoc
            })
            if (results == null) {
                return this.res(false, null)
            } else {
                return this.res(true, results)
            }
        } catch(error) {
            console.log('[ERROR - READ ONE] '+error.message)
            return this.res(false, error.message)
        }
    }

    async create(data, conditions = null, assoc=[]) {
        try {
            data.isActive = true
            let results = await this.instance.create(data)
            return this.res(true, results)
        } catch(error) {
            console.log('[ERROR - UPDATE] '+error.message)
            return this.res(false, error.message)
        }
    }

    async update(data, conditions = null, assoc=[]) {
        try {
            let results = await this.instance.update(data, {
                where: conditions,
                returning: true
            })
            return this.res(true, results)
        } catch(error) {
            console.log('[ERROR - UPDATE] '+error.message)
            return this.res(false, error.message)
        }
    }

    async delete(conditions = null) {
        try {
            let results = await this.instance.destroy({
                where: conditions,
            })
            return this.res(true, results)
        } catch(error) {
            console.log('[ERROR - DELETE] '+error.message)
            return this.res(false, error.message)
        }
    }
}
export default new CartRepository()