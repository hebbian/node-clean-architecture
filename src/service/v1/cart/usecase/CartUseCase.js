import express from 'express'
import CartRepository from '../repository/CartRepository'
import ResponseMessage from '../../../../../helper/ResponseMessage'

class CartUseCase {
    validation(req, res, next) {
        //ResponseMessage.doResponse(res, 400, 'data error fetched')
        return next()
    }
    async getAllCart(req, res) {
        try {
            let result = await CartRepository.readAll()
            //let meta = ResponseMessage.setMeta(res, 0,0,0,0,0)
            let response = ResponseMessage.doResponse(200, 'data successfully fetched', result)
            res.json(response)
        } catch(error) {
            console.log(error)
            let response = ResponseMessage.doResponse(400, 'data error fetched')
            res.status(400).send(response)
        }
    }
}
export default new CartUseCase()