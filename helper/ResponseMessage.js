class ResponseMessage {
    cleanObj(obj) {
        Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key])
        return obj
    }
    doResponse(code, message, data=null, meta=null) {
        let obj = {
            code: code,
            message: message,
            data: data,
            meta: meta
        }
        let cleanObj = this.cleanObj(obj)
        return cleanObj
    }
    setMeta(page=null, limit=null, currentPage=null, totalPage=null, totalRecord=null) {
        let obj = {
            page: page,
            limit: limit,
            currentPage: currentPage,
            totalPage: totalPage,
            totalRecord: totalRecord
        }
        let cleanObj = this.cleanObj(obj)
        return cleanObj
    }
}
export default new ResponseMessage()